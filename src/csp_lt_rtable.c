#include "csp_lt_rtable.h"

#include <csp_lt.h>

// ugly hack so that we can return pointer from csp_lt_rtable_find_route
static csp_lt_route_t _rt;

static int csp_lt_rtable_cb_wrapper(csp_lt_persist_cb_t cb, csp_lt_rtable_entry_t* rtable) {
	if(cb == NULL) {
		return(CSP_LT_ERR_NOMEM);
	}
	cb(rtable);
	return(CSP_LT_ERR_NONE);
}

int csp_lt_rtable_set(csp_lt_inst_t *inst, uint8_t address, uint8_t netmask, csp_lt_iface_t *ifc, uint8_t via) {
	if(address > CSP_LT_ID_HOST_MAX) {
		return(CSP_LT_ERR_INVAL);
	}

	if(netmask > CSP_LT_ID_HOST_SIZE) {
		return(CSP_LT_ERR_INVAL);
	}

	size_t len = tl_strlen(ifc->name);
	if(len > CSP_LT_IFLIST_NAME_MAX) {
		return(CSP_LT_ERR_NOMEM);
	}

	// get netmask base
	uint8_t maskBase = (address & 0x1F) & (0xFF << (CSP_LT_ID_HOST_SIZE - netmask));

	// get number of addresses in range
	uint8_t maskRange = 1 << (CSP_LT_ID_HOST_SIZE - netmask);

	// get persistent rtable
	int state = csp_lt_rtable_cb_wrapper(inst->rtable_get_cb, inst->rtable);
	if(state != CSP_LT_ERR_NONE) {
		return(state);
	}

	// set routes based on the mask
	for(uint8_t i = 0; i < maskRange; i++) {
		tl_strncpy(inst->rtable[maskBase + i].name, ifc->name, len);
		inst->rtable[maskBase + i].name[len] = '\0';
		inst->rtable[maskBase + i].via = via;
	}

	// update persistent rtable
	return(csp_lt_rtable_cb_wrapper(inst->rtable_set_cb, inst->rtable));
}

csp_lt_route_t * csp_lt_rtable_find_route(csp_lt_inst_t *inst, uint8_t dest_address) {
	// get persistent rtable
	int state = csp_lt_rtable_cb_wrapper(inst->rtable_get_cb, inst->rtable);
	if(state != CSP_LT_ERR_NONE) {
		return(NULL);
	}

	// check address match
	csp_lt_iface_t* ifc = csp_lt_iflist_get_by_name(inst, inst->rtable[dest_address].name);
	if(ifc != NULL) {
		_rt.iface = ifc;
		_rt.via = inst->rtable[dest_address].via;
		return(&_rt);
	}

	// check default route match
	ifc = csp_lt_iflist_get_by_name(inst, inst->rtable[CSP_LT_DEFAULT_ROUTE].name);
	if(ifc != NULL) {
		_rt.iface = ifc;
		_rt.via = inst->rtable[CSP_LT_DEFAULT_ROUTE].via;
		return(&_rt);
	}

	// big bad
	return(NULL);
}

void csp_lt_rtable_set_def_route(csp_lt_inst_t *inst, csp_lt_iface_t *iface) {
	// get persistent rtable
	int state = csp_lt_rtable_cb_wrapper(inst->rtable_get_cb, inst->rtable);
	if(state != CSP_LT_ERR_NONE) {
		return;
	}

	size_t len = tl_strlen(iface->name);
	if(len > CSP_LT_IFLIST_NAME_MAX) {
		return;
	}

	// update default route
	tl_strncpy(inst->rtable[CSP_LT_DEFAULT_ROUTE].name, iface->name, len);
	inst->rtable[CSP_LT_DEFAULT_ROUTE].name[len] = '\0';
	inst->rtable[CSP_LT_DEFAULT_ROUTE].via = CSP_LT_NO_VIA_ADDRESS;

	// set persistent rtable
	csp_lt_rtable_cb_wrapper(inst->rtable_set_cb, inst->rtable);
}
