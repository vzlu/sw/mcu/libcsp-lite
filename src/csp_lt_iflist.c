#include "csp_lt_iflist.h"

#include <tinylibc.h>

#include <csp_lt.h>

csp_lt_iface_t* csp_lt_iflist_get_by_name(csp_lt_inst_t *inst, const char *name) {
	if(name == NULL) {
		return(NULL);
	}

	for(uint8_t i = 0; i < inst->num_ifcs; i++) {
    if(tl_strncmp(name, inst->interfaces[i]->name, CSP_LT_IFLIST_NAME_MAX) == 0) {
      return(inst->interfaces[i]);
    }
  }
  return(NULL);
}

int csp_lt_iflist_add(csp_lt_inst_t *inst, csp_lt_iface_t *ifc) {
	if(inst->num_ifcs > CSP_LT_ID_HOST_MAX) {
		return(CSP_LT_ERR_NOMEM);
	}

	if(ifc == NULL) {
		return(CSP_LT_ERR_INVAL);
	}

	// add ifc to list
	inst->interfaces[inst->num_ifcs++] = ifc;
	return(CSP_LT_ERR_NONE);
}
