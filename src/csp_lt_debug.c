#include "csp_lt_debug.h"

void csp_lt_debug_packet(csp_lt_packet_t* packet) {
  CSP_LT_DEBUG_PRINTF("host:%d->%d,", packet->id.src, packet->id.dst);
  CSP_LT_DEBUG_PRINTF("port:%d->%d,", packet->id.sport, packet->id.dport);
  CSP_LT_DEBUG_PRINTF("flags:%02x", packet->id.flags);
  if(packet->length > 0) {
    CSP_LT_DEBUG_PRINTF(",data[%d]:\n", packet->length);
    for(int i = 0; i < packet->length; i++) {
      CSP_LT_DEBUG_PRINTF("%02x ", packet->data[i]);
    }
    CSP_LT_DEBUG_PRINTF("\n");
  } else {
    CSP_LT_DEBUG_PRINTF("\n");
  }
}
