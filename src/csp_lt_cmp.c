#include "csp_lt_cmp.h"

#include <tinylibc.h>

#include <csp_lt_cmp.h>
#include <csp_lt_debug.h>
#include <csp_lt_endian.h>
#include <csp_lt_error.h>

static int do_cmp_ident(csp_lt_inst_t *inst, struct csp_lt_cmp_message *cmp) {
  /* Copy revision */
  tl_sprintf(cmp->ident.revision, "%s lite", csp_lt_get_revision(inst));
  cmp->ident.revision[CSP_LT_CMP_IDENT_REV_LEN - 1] = '\0';

  /* Copy compilation date */
  tl_strncpy(cmp->ident.date, __DATE__, CSP_LT_CMP_IDENT_DATE_LEN);
  cmp->ident.date[CSP_LT_CMP_IDENT_DATE_LEN - 1] = '\0';

  /* Copy compilation time */
  tl_strncpy(cmp->ident.time, __TIME__, CSP_LT_CMP_IDENT_TIME_LEN);
  cmp->ident.time[CSP_LT_CMP_IDENT_TIME_LEN - 1] = '\0';

  /* Copy hostname */
  tl_strncpy(cmp->ident.hostname, csp_lt_get_hostname(inst), CSP_LT_HOSTNAME_LEN);
  cmp->ident.hostname[CSP_LT_HOSTNAME_LEN - 1] = '\0';

  /* Copy model name */
  tl_strncpy(cmp->ident.model, csp_lt_get_model(inst), CSP_LT_MODEL_LEN);
  cmp->ident.model[CSP_LT_MODEL_LEN - 1] = '\0';

  return(CSP_LT_ERR_NONE);
}

static int do_cmp_route_set(csp_lt_inst_t *inst, struct csp_lt_cmp_message *cmp) {
  csp_lt_iface_t* ifc = csp_lt_iflist_get_by_name(inst, cmp->route_set.interface);
  if(ifc == NULL) {
    return(CSP_LT_ERR_INVAL);
  }

  csp_lt_rtable_set(inst, cmp->route_set.dest_node, CSP_LT_ID_HOST_SIZE, ifc, cmp->route_set.next_hop_via);
  return(CSP_LT_ERR_NONE);
}

static int do_cmp_if_stats(csp_lt_inst_t *inst, struct csp_lt_cmp_message *cmp) {
  csp_lt_iface_t* ifc = csp_lt_iflist_get_by_name(inst, cmp->if_stats.interface);
  if(ifc == NULL) {
    return(CSP_LT_ERR_INVAL);
  }

  cmp->if_stats.tx =       csp_lt_hton32(ifc->tx);
  cmp->if_stats.rx =       csp_lt_hton32(ifc->rx);
  cmp->if_stats.tx_error = csp_lt_hton32(ifc->tx_error);
  cmp->if_stats.rx_error = csp_lt_hton32(ifc->rx_error);
  cmp->if_stats.drop =     csp_lt_hton32(ifc->drop);
  cmp->if_stats.autherr =  csp_lt_hton32(ifc->autherr);
  cmp->if_stats.frame =    csp_lt_hton32(ifc->frame);
  cmp->if_stats.txbytes =  csp_lt_hton32(ifc->txbytes);
  cmp->if_stats.rxbytes =  csp_lt_hton32(ifc->rxbytes);
  cmp->if_stats.irq = 	   csp_lt_hton32(ifc->irq);
  return(CSP_LT_ERR_NONE);
}

static int do_cmp_peek(struct csp_lt_cmp_message *cmp) {
  cmp->peek.addr = csp_lt_hton32(cmp->peek.addr);
  if (cmp->peek.len > CSP_LT_CMP_PEEK_MAX_LEN)
      return CSP_LT_ERR_INVAL;

  /* Dangerous, you better know what you are doing */
  //csp_lt_cmp_memcpy_fnc((csp_lt_memptr_t) (uintptr_t) cmp->peek.data, (csp_lt_memptr_t) (unsigned long) cmp->peek.addr, cmp->peek.len);

  return(CSP_LT_ERR_NONE);
}

static int do_cmp_poke(struct csp_lt_cmp_message *cmp) {
  cmp->poke.addr = csp_lt_hton32(cmp->poke.addr);
  if(cmp->poke.len > CSP_LT_CMP_POKE_MAX_LEN) {
    return(CSP_LT_ERR_INVAL);
  }

  /* Extremely dangerous, you better know what you are doing */
  //csp_lt_cmp_memcpy_fnc((csp_lt_memptr_t) (unsigned long) cmp->poke.addr, (csp_lt_memptr_t) (uintptr_t) cmp->poke.data, cmp->poke.len);

  return(CSP_LT_ERR_NONE);
}

static int do_cmp_clock(struct csp_lt_cmp_message *cmp) {
  cmp->clock.tv_sec = csp_lt_ntoh32(cmp->clock.tv_sec);
  cmp->clock.tv_nsec = csp_lt_ntoh32(cmp->clock.tv_nsec);

  if (cmp->clock.tv_sec != 0) {
      //clock_set_time(&cmp->clock);
  }

  //clock_get_time(&cmp->clock);
  cmp->clock.tv_sec = csp_lt_hton32(cmp->clock.tv_sec);
  cmp->clock.tv_nsec = csp_lt_hton32(cmp->clock.tv_nsec);
  return(CSP_LT_ERR_NONE);
}

int csp_lt_cmp_handler(csp_lt_inst_t *inst) {
  int ret = CSP_LT_ERR_INVAL;
  struct csp_lt_cmp_message * cmp = (struct csp_lt_cmp_message *) inst->packet->data;

  /* Ignore everything but requests */
  if(cmp->type != CSP_LT_CMP_REQUEST) {
    return(ret);
  }

  switch(cmp->code) {
    case CSP_LT_CMP_IDENT:
      ret = do_cmp_ident(inst, cmp);
      inst->packet->length = CMP_LT_SIZE(ident);
      break;

    case CSP_LT_CMP_ROUTE_SET:
      ret = do_cmp_route_set(inst, cmp);
      inst->packet->length = CMP_LT_SIZE(route_set);
      break;

    case CSP_LT_CMP_IF_STATS:
      ret = do_cmp_if_stats(inst, cmp);
      inst->packet->length = CMP_LT_SIZE(if_stats);
      break;

    case CSP_LT_CMP_PEEK:
      ret = do_cmp_peek(cmp);
      break;

    case CSP_LT_CMP_POKE:
      ret = do_cmp_poke(cmp);
      break;

    case CSP_LT_CMP_CLOCK:
      ret = do_cmp_clock(cmp);
      break;

    default:
      ret = CSP_LT_ERR_INVAL;
      break;
  }

  cmp->type = CSP_LT_CMP_REPLY;

  return(ret);
}
