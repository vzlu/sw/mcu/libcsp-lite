#include <tinylibc.h>

#include <csp_lt.h>
#include <csp_lt_cmp.h>
#include <csp_lt_crc32.h>
#include <csp_lt_endian.h>

#if defined(CSP_LT_RTOS)
  #include <FreeRTOS.h>
  #include <task.h>
#endif

// maximum allowed packet data length
#define CSP_LT_DATA_SIZE                 (CSP_LT_BUFF_SIZE - CSP_LT_HEADER_LENGTH - 2)

int16_t csp_lt_default_handlers(csp_lt_inst_t *inst) {
  switch(inst->packet->id.dport) {
    case CSP_LT_CMP: {
      return(csp_lt_cmp_handler(inst));
    } break;

    case CSP_LT_PING: {
      return(CSP_LT_ERR_NONE);
    } break;

    case CSP_LT_PS: {
      #if defined(CSP_LT_RTOS)
        //FIXME implement this without using vTaskList
        char buff[] = "not implemented";
      #else
        char buff[] = "not implemented";
      #endif
      tl_strcpy((char*)(inst->packet->data), buff);
      inst->packet->length = tl_strlen(buff);
      inst->packet->data[inst->packet->length] = '\0';
      return(CSP_LT_ERR_NONE);
    } break;

    case CSP_LT_MEMFREE: {
      uint32_t total = 0;
      if(inst->memfree_cb) {
        total = (uint32_t)inst->memfree_cb();
      }
      total = csp_lt_hton32(total);
      tl_memcpy(inst->packet->data, (const uint8_t *)&total, sizeof(total));
      inst->packet->length = sizeof(total);
      return(CSP_LT_ERR_NONE);
    } break;

    case CSP_LT_REBOOT: {
      uint32_t magic_word;
      tl_memcpy((uint8_t *)&magic_word, inst->packet->data, sizeof(magic_word));
      magic_word = csp_lt_ntoh32(magic_word);
      if((magic_word == CSP_LT_REBOOT_MAGIC) && (inst->reboot_cb)) {
        inst->reboot_cb();
      }
      return(CSP_LT_ERR_NONE);
    } break;

    case CSP_LT_BUF_FREE: {
      uint32_t size = 1;
      size = csp_lt_hton32(size);
      tl_memcpy(inst->packet->data, (const uint8_t *)&size, sizeof(size));
      inst->packet->length = sizeof(size);
      return(CSP_LT_ERR_NONE);
    } break;

    case CSP_LT_UPTIME: {
      uint32_t time = tl_ticks() / tl_get_tickrate();
      time = csp_lt_hton32(time);
      tl_memcpy(inst->packet->data, (const uint8_t *)&time, sizeof(time));
      inst->packet->length = sizeof(time);
      return(CSP_LT_ERR_NONE);
    }
  }

  return(1);
}

void csp_lt_rx_handler(csp_lt_inst_t *inst, uint8_t *buff, uint16_t len) {
  // check there is enough data at least for the header
  if(len < CSP_LT_HEADER_LENGTH) {
    return;
  }

  // check the reported length also does not exceed the buffer size
  if(len > (CSP_LT_BUFF_SIZE - sizeof(inst->packet->length))) {
    return;
  }

  tl_memcpy(&(inst->packet->id.ext), buff, len);
  inst->packet->id.ext = csp_lt_ntoh32(inst->packet->id.ext);
  inst->packet->length = len - CSP_LT_HEADER_LENGTH;

  CSP_LT_DEBUG_PRINTF("[CSP] IN  ");
  CSP_LT_DEBUG_PACKET(inst->packet);

  // check if we can route this
  csp_lt_route_t* route = csp_lt_rtable_find_route(inst, inst->packet->id.src);
  if((inst->packet->id.dst != inst->address) && (route == NULL)) {
    CSP_LT_DEBUG_PRINTF("[CSP] Rx unknown route!\n");
    return;
  }

  CSP_LT_DEBUG_PRINTF("[CSP] IFC %s\n", route->iface->name);

  // check CRC
  if((inst->packet->id.flags & CSP_LT_FCRC32) || (route->iface->force_flags & CSP_LT_FCRC32)) {
    if(csp_lt_crc32_verify(inst->packet) != CSP_LT_ERR_NONE) {
      route->iface->rx_error++;
      CSP_LT_DEBUG_PRINTF("[CSP] CRC check failed!\n");
      return;
    }
  }

  // check if this packet is for this node
  if(inst->packet->id.dst == inst->address) {
    route->iface->rx++;
    route->iface->rxbytes += len;

    // default handlers
    int16_t ret = csp_lt_default_handlers(inst);
    if(ret <= CSP_LT_ERR_NONE) {
      CSP_LT_DEBUG_PRINTF("[CSP] Executed default handler, status %d\n", ret);
      csp_lt_reply(inst);
      return;
    }

    // service the request
    if(inst->service_cb) {
      inst->service_cb(inst);
    }
    return;
  }

  // route all other packets
  if(csp_lt_send(inst)) {
    route->iface->rx++;
    route->iface->rxbytes += len;
  }
}

int16_t csp_lt_init(csp_lt_inst_t *inst, uint8_t address) {
  if(address > CSP_LT_ID_HOST_MAX) {
    return(CSP_LT_ERR_INVAL);
  }

  inst->address = address;

  #if defined(CSP_LT_CRC_RAM_LUT)
  csp_lt_crc32_gen_lut();
  #endif

  inst->packet = (csp_lt_packet_t *)inst->packet_buff;
  return(CSP_LT_ERR_NONE);
}

int16_t csp_lt_reply(csp_lt_inst_t *inst) {
  if(inst->packet == NULL) {
    return(0);
  }

  uint8_t tmp;
  tmp = inst->packet->id.src;
  inst->packet->id.src = inst->packet->id.dst;
  inst->packet->id.dst = tmp;
  tmp = inst->packet->id.sport;
  inst->packet->id.sport = inst->packet->id.dport;
  inst->packet->id.dport = tmp;
  return(csp_lt_send(inst));
}

int16_t csp_lt_send(csp_lt_inst_t *inst) {
  if(inst->packet == NULL) {
    return(0);
  }

  // check if we can route this
  csp_lt_route_t* route = csp_lt_rtable_find_route(inst, inst->packet->id.dst);
  if(route == NULL) {
    CSP_LT_DEBUG_PRINTF("[CSP] Tx unknown route!\n");
    return(0);
  }

  if((inst->packet->id.flags & CSP_LT_FCRC32) || (route->iface->force_flags & CSP_LT_FCRC32)) {
    csp_lt_crc32_append(inst->packet);
  }

  CSP_LT_DEBUG_PRINTF("[CSP] IFC %s\n", route->iface->name);
  CSP_LT_DEBUG_PRINTF("[CSP] OUT ");
  CSP_LT_DEBUG_PACKET(inst->packet);

  uint8_t via = route->via;
  if(route->via == CSP_LT_NO_VIA_ADDRESS) {
    via = inst->packet->id.dst;
  }
  CSP_LT_DEBUG_PRINTF("[CSP] VIA %d\n", via);

  route->iface->tx++;
  route->iface->txbytes += (inst->packet->length + CSP_LT_HEADER_LENGTH);

  // check loopback messages to self
  if(inst->packet->id.dst == inst->address) {
    inst->packet->id.ext = csp_lt_hton32(inst->packet->id.ext);
    csp_lt_rx_handler(inst, (uint8_t*)(&(inst->packet)->id.ext), inst->packet->length + CSP_LT_HEADER_LENGTH);
    return(CSP_LT_ERR_NONE);
  }

  // not a loopback message, send it out
  inst->packet->id.ext = csp_lt_hton32(inst->packet->id.ext);
  return(route->iface->send_cb(via, (uint8_t*)(&(inst->packet)->id.ext), inst->packet->length + CSP_LT_HEADER_LENGTH));
}

void csp_lt_set_hostname(csp_lt_inst_t *inst, const char *name) {
  tl_strncpy(inst->hostname, name, CSP_LT_HOSTNAME_LEN);
}

void csp_lt_set_model(csp_lt_inst_t *inst, const char *model) {
  tl_strncpy(inst->model, model, CSP_LT_MODEL_LEN);
}

void csp_lt_set_revision(csp_lt_inst_t *inst, const char *rev) {
  tl_strncpy(inst->revision, rev, CSP_LT_REVISION_LEN);
}

const char *csp_lt_get_hostname(csp_lt_inst_t *inst) {
  return(inst->hostname);
}

const char *csp_lt_get_model(csp_lt_inst_t *inst) {
  return(inst->model);
}

const char* csp_lt_get_revision(csp_lt_inst_t *inst) {
  return(inst->revision);
}

void *csp_lt_packet_data(csp_lt_inst_t *inst, size_t capacity) {
  if(capacity >= CSP_LT_DATA_SIZE) {
    return(NULL);
  }

  return(inst->packet->data);
}

csp_lt_packet_t* csp_lt_get_packet_ptr(csp_lt_inst_t *inst) {
  return(inst->packet);
}
