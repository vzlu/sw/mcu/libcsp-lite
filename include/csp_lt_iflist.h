#ifndef CSP_LT_IFLIST_H_
#define CSP_LT_IFLIST_H_

#include <csp_lt_interface.h>
#include <csp_lt_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
   Add interface to the list.

   @param[in] iface interface. The interface must remain valid as long as the application is running.
   @return #CSP_LT_ERR_NONE on success, otherwise an error code.
*/
int csp_lt_iflist_add(csp_lt_inst_t *inst, csp_lt_iface_t * iface);

/**
   Get interface by name.

   @param[in] name interface name.
   @return Interface or NULL if not found.
*/
csp_lt_iface_t* csp_lt_iflist_get_by_name(csp_lt_inst_t *inst, const char *name);

#ifdef __cplusplus
}
#endif
#endif
