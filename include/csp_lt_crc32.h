#ifndef _CSP_LT_CRC32_H_
#define _CSP_LT_CRC32_H_

#include <csp_lt.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Append CRC32 checksum to packet
 * @param packet Packet to append checksum
 * @return 0 on success, -1 on error
 */
int csp_lt_crc32_append(csp_lt_packet_t * packet);

/**
 * Verify CRC32 checksum on packet
 * @param packet Packet to verify
 * @return 0 if checksum is valid, -1 otherwise
 */
int csp_lt_crc32_verify(csp_lt_packet_t * packet);

/**
 * Calculate checksum for a given memory area
 * @param data pointer to memory
 * @param length length of memory to do checksum on
 * @return return uint32_t checksum
 */
uint32_t csp_lt_crc32_memory(const uint8_t * data, uint32_t length);

#if defined(CSP_LT_CRC_RAM_LUT)
void csp_lt_crc32_gen_lut();
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _CSP_LT_CRC32_H_ */
