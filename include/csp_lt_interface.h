/*
Cubesat Space Protocol - A small network-layer protocol designed for Cubesats
Copyright (C) 2012 Gomspace ApS (http://www.gomspace.com)
Copyright (C) 2012 AAUSAT3 Project (http://aausat3.space.aau.dk)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _CSP_LT_INTERFACE_H_
#define _CSP_LT_INTERFACE_H_

#include <stdint.h>

/**
   @file
   Interface.
*/

#ifdef __cplusplus
extern "C" {
#endif

/**
   Max unique length of interface name, when matching names.
*/
#define CSP_LT_IFLIST_NAME_MAX    10

typedef int (*csp_lt_iface_send_cb_t)(uint8_t via, uint8_t* buff, uint16_t len);

/**
   CSP interface.
*/
typedef struct {
	const char *name;			//!< Interface name, name should not exceed #CSP_LT_IFLIST_NAME_MAX
	csp_lt_iface_send_cb_t send_cb;
	uint32_t force_flags;
	uint32_t tx;					//!< Successfully transmitted packets
	uint32_t rx;					//!< Successfully received packets
	uint32_t tx_error;		//!< unused - Transmit errors
	uint32_t rx_error;		//!< Receive errors, e.g. too large message
	uint32_t drop;				//!< unused - Dropped packets
	uint32_t autherr; 		//!< unused - Authentication errors
	uint32_t frame;				//!< unused - Frame format errors
	uint32_t txbytes;			//!< Transmitted bytes
	uint32_t rxbytes;			//!< Received bytes
	uint32_t irq;					//!< unused - Interrupts
} csp_lt_iface_t;
#ifdef __cplusplus
}
#endif
#endif
