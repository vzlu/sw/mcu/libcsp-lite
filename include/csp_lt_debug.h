#ifndef CSP_LT_DEBUG_H
#define CSP_LT_DEBUG_H

#include <tinylibc.h>

#include <csp_lt_types.h>

#ifdef CSP_LT_DEBUG
  #define CSP_LT_DEBUG_PRINTF(...) { tl_printf(__VA_ARGS__); }
  #define CSP_LT_DEBUG_PACKET(...) { csp_lt_debug_packet(__VA_ARGS__); }
#else
  #define CSP_LT_DEBUG_PRINTF(...) {}
  #define CSP_LT_DEBUG_PACKET(...) {}
#endif

void csp_lt_debug_packet(csp_lt_packet_t* packet);

#endif
