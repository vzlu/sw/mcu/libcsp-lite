#ifndef CSP_LT_TYPES_H_
#define CSP_LT_TYPES_H_

/**
   @file
   Basic types.
*/

#include <stdint.h>
#include <stddef.h>

#include <csp_lt_interface.h>

#ifdef __cplusplus
extern "C" {
#endif

#if (CSP_LT_BIG_ENDIAN && CSP_LT_LITTLE_ENDIAN)
#error "Only define/set either CSP_LT_BIG_ENDIAN or CSP_LT_LITTLE_ENDIAN"
#endif

/**
   Reserved ports for CSP services.
*/
typedef enum {
	CSP_LT_CMP				= 0,   //!< CSP management, e.g. memory, routes, stats
	CSP_LT_PING			= 1,   //!< Ping - return ping
	CSP_LT_PS				= 2,   //!< Current process list
	CSP_LT_MEMFREE			= 3,   //!< Free memory
	CSP_LT_REBOOT			= 4,   //!< Reboot, see #CSP_LT_REBOOT_MAGIC and #CSP_LT_REBOOT_SHUTDOWN_MAGIC
	CSP_LT_BUF_FREE			= 5,   //!< Free CSP buffers
	CSP_LT_UPTIME			= 6,   //!< Uptime
} csp_lt_service_port_t;

/** Listen on all ports, primarily used with csp_lt_bind() */
#define CSP_LT_ANY				255

/**
   Message priority.
*/
typedef enum {
	CSP_LT_PRIO_CRITICAL		= 0, //!< Critical
	CSP_LT_PRIO_HIGH			= 1, //!< High
	CSP_LT_PRIO_NORM			= 2, //!< Normal (default)
	CSP_LT_PRIO_LOW			= 3, //!< Low
} csp_lt_prio_t;

#define CSP_LT_PRIORITIES			(1 << CSP_LT_ID_PRIO_SIZE) //!< Number of CSP message priorities.

/**
   @defgroup CSP_LT_HEADER_DEF CSP header definition.
   @{
*/
#define CSP_LT_ID_PRIO_SIZE		2 //!< Bits for priority, see #csp_lt_prio_t
#define CSP_LT_ID_HOST_SIZE		5 //!< Bits for host (destination/source address)
#define CSP_LT_ID_PORT_SIZE		6 //!< Bits for port (destination/source port)
#define CSP_LT_ID_FLAGS_SIZE		8 //!< Bits for flags, see @ref CSP_LT_HEADER_FLAGS

/** Number of bits in CSP header */
#define CSP_LT_HEADER_BITS			(CSP_LT_ID_PRIO_SIZE + (2 * CSP_LT_ID_HOST_SIZE) + (2 * CSP_LT_ID_PORT_SIZE) + CSP_LT_ID_FLAGS_SIZE)
/** CSP header size in bytes */
#define CSP_LT_HEADER_LENGTH		(CSP_LT_HEADER_BITS / 8)

#if CSP_LT_HEADER_BITS != 32 && __GNUC__
#error "Header length must be 32 bits"
#endif

#define CSP_LT_ID_PRIO_MAX			((1 << (CSP_LT_ID_PRIO_SIZE)) - 1)  //!< Max priority value in header
#define CSP_LT_ID_HOST_MAX			((1 << (CSP_LT_ID_HOST_SIZE)) - 1)  //!< Max host value in header
#define CSP_LT_ID_PORT_MAX			((1 << (CSP_LT_ID_PORT_SIZE)) - 1)  //!< Max port value in header
#define CSP_LT_ID_FLAGS_MAX		((1 << (CSP_LT_ID_FLAGS_SIZE)) - 1) //!< Max flag(s) value in header

/** CSP identifier/header - priority mask */
#define CSP_LT_ID_PRIO_MASK		((uint32_t) CSP_LT_ID_PRIO_MAX  << (CSP_LT_ID_FLAGS_SIZE + (2 * CSP_LT_ID_PORT_SIZE) + (2 * CSP_LT_ID_HOST_SIZE)))
/** CSP identifier/header - source address mask */
#define CSP_LT_ID_SRC_MASK	 		((uint32_t) CSP_LT_ID_HOST_MAX  << (CSP_LT_ID_FLAGS_SIZE + (2 * CSP_LT_ID_PORT_SIZE) + (1 * CSP_LT_ID_HOST_SIZE)))
/** CSP identifier/header - destination address mask */
#define CSP_LT_ID_DST_MASK	 		((uint32_t) CSP_LT_ID_HOST_MAX  << (CSP_LT_ID_FLAGS_SIZE + (2 * CSP_LT_ID_PORT_SIZE)))
/** CSP identifier/header - destination port mask */
#define CSP_LT_ID_DPORT_MASK   		((uint32_t) CSP_LT_ID_PORT_MAX  << (CSP_LT_ID_FLAGS_SIZE + (1 * CSP_LT_ID_PORT_SIZE)))
/** CSP identifier/header - source port mask */
#define CSP_LT_ID_SPORT_MASK   		((uint32_t) CSP_LT_ID_PORT_MAX  << (CSP_LT_ID_FLAGS_SIZE))
/** CSP identifier/header - flag mask */
#define CSP_LT_ID_FLAGS_MASK		((uint32_t) CSP_LT_ID_FLAGS_MAX << (0))
/** CSP identifier/header - connection mask (source & destination address + source & destination ports) */
#define CSP_LT_ID_CONN_MASK		(CSP_LT_ID_SRC_MASK | CSP_LT_ID_DST_MASK | CSP_LT_ID_DPORT_MASK | CSP_LT_ID_SPORT_MASK)
/**@}*/

/**
   CSP identifier/header.
   This union is sent directly on the wire, hence the big/little endian definitions
*/
typedef union {
    /** Entire identifier. */
    uint32_t ext;
    /** Individual fields. */
    struct __attribute__((__packed__)) {
#if (CSP_LT_BIG_ENDIAN)
        unsigned int pri   : CSP_LT_ID_PRIO_SIZE;  //!< Priority
        unsigned int src   : CSP_LT_ID_HOST_SIZE;  //!< Source address
        unsigned int dst   : CSP_LT_ID_HOST_SIZE;  //!< Destination address
        unsigned int dport : CSP_LT_ID_PORT_SIZE;  //!< Destination port
        unsigned int sport : CSP_LT_ID_PORT_SIZE;  //!< Source port
        unsigned int flags : CSP_LT_ID_FLAGS_SIZE; //!< Flags, see @ref CSP_LT_HEADER_FLAGS
#elif (CSP_LT_LITTLE_ENDIAN)
        unsigned int flags : CSP_LT_ID_FLAGS_SIZE;
        unsigned int sport : CSP_LT_ID_PORT_SIZE;
        unsigned int dport : CSP_LT_ID_PORT_SIZE;
        unsigned int dst   : CSP_LT_ID_HOST_SIZE;
        unsigned int src   : CSP_LT_ID_HOST_SIZE;
        unsigned int pri   : CSP_LT_ID_PRIO_SIZE;
#endif
    };
} csp_lt_id_t;

/** Broadcast address */
#define CSP_LT_BROADCAST_ADDR		CSP_LT_ID_HOST_MAX

/** Default routing address */
#define CSP_LT_DEFAULT_ROUTE		(CSP_LT_ID_HOST_MAX + 1)

/**
   @defgroup CSP_LT_HEADER_FLAGS CSP header flags.
   @{
*/
#define CSP_LT_FRES1			0x80 //!< Reserved for future use
#define CSP_LT_FRES2			0x40 //!< Reserved for future use
#define CSP_LT_FRES3			0x20 //!< Reserved for future use
#define CSP_LT_FFRAG			0x10 //!< Use fragmentation
#define CSP_LT_FHMAC			0x08 //!< Use HMAC verification
#define CSP_LT_FXTEA			0x04 //!< Use XTEA encryption
#define CSP_LT_FRDP			0x02 //!< Use RDP protocol
#define CSP_LT_FCRC32			0x01 //!< Use CRC32 checksum
/**@}*/


/**
   CSP Packet.

   This structure is constructed to fit with all interface and protocols to prevent the need to copy data.

   @note In most cases a CSP packet cannot be reused in case of send failure, because the lower layers may add additional data causing
   increased length (e.g. CRC32), convert the CSP id/ to different endian (e.g. I2C), etc.
*/
typedef struct __attribute__((__packed__)) {
        /** Data length. Must be just before CSP ID. */
	uint16_t length;
	/** CSP id. Must be just before data, as it allows the interface to id and data in a single operation. */
	csp_lt_id_t id;
	/**
           Data part of packet.
           When using the csp_lt_buffer API, the data part is set by csp_lt_buffer_init(), and can later be accessed by csp_lt_buffer_data_size()
        */
	union {
		/** Access data as uint8_t. */
		uint8_t data[0];
		/** Access data as uint16_t */
		uint16_t data16[0];
		/** Access data as uint32_t */
		uint32_t data32[0];
	};
} csp_lt_packet_t;

/**
   Size of the packet overhead in #csp_lt_packet_t.
   The overhead is the difference between the total buffer size (returned by csp_lt_buffer_size()) and the data part
   of the #csp_lt_packet_t (returned by csp_lt_buffer_data_size()).
*/
#define CSP_LT_BUFFER_PACKET_OVERHEAD      (sizeof(csp_lt_packet_t) - sizeof(((csp_lt_packet_t *)0)->data))

/** Max length of host name - including zero termination */
#define CSP_LT_HOSTNAME_LEN	20
/** Max length of model name - including zero termination */
#define CSP_LT_MODEL_LEN		30
/** Max length of model name - including zero termination, excluding overheads */
#define CSP_LT_REVISION_LEN		6

/** Magic number for reboot request, for service-code #CSP_LT_REBOOT */
#define CSP_LT_REBOOT_MAGIC		0x80078007
/** Magic number for shutdown request, for service-code #CSP_LT_REBOOT */
#define CSP_LT_REBOOT_SHUTDOWN_MAGIC	0xD1E5529A

/**
   Route structure.
   @see csp_lt_rtable_find_route().
*/
typedef struct {
    /** Which interface to route packet on */
    csp_lt_iface_t * iface;
    /** If different from #CSP_LT_NO_VIA_ADDRESS, send packet to this address, instead of the destination address in the CSP header. */
    uint8_t via;
} csp_lt_route_t;

/**
   Route entry.
   @see csp_lt_rtable_find_route().
*/
typedef struct {
    /** Which interface to route packet on */
    char name[CSP_LT_IFLIST_NAME_MAX];
    /** If different from #CSP_LT_NO_VIA_ADDRESS, send packet to this address, instead of the destination address in the CSP header. */
    uint8_t via;
} csp_lt_rtable_entry_t;

typedef struct csp_lt_inst_t csp_lt_inst_t;
typedef void (*csp_lt_service_cb_t)(csp_lt_inst_t *inst);
typedef void (*csp_lt_reboot_cb_t)(void);
typedef int (*csp_lt_memfree_cb_t)(void);
typedef void (*csp_lt_persist_cb_t)(csp_lt_rtable_entry_t *rtable);

// OOP in C, the final proof that God is dead and we have killed Him
struct csp_lt_inst_t {
  // address of this CSP instance
  uint8_t address;

  // callbacks
	csp_lt_service_cb_t service_cb;
	csp_lt_reboot_cb_t reboot_cb;
	csp_lt_memfree_cb_t memfree_cb;
  csp_lt_persist_cb_t rtable_set_cb;
  csp_lt_persist_cb_t rtable_get_cb;

  // singleton packet
	csp_lt_packet_t *packet;
	uint8_t packet_buff[CSP_LT_BUFF_SIZE];

  // CMP buffers
	char hostname[CSP_LT_HOSTNAME_LEN];
	char model[CSP_LT_MODEL_LEN];
	char revision[CSP_LT_REVISION_LEN];

  // routing table
	// TODO how to ensure this gets initialized to 0
  csp_lt_rtable_entry_t rtable[CSP_LT_DEFAULT_ROUTE + 1];

	// interface list
	uint8_t num_ifcs;
	csp_lt_iface_t* interfaces[CSP_LT_ID_HOST_MAX + 1];
};

#ifdef __cplusplus
}
#endif
#endif /* CSP_LT_TYPES_H_ */
