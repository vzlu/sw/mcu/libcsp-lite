/*
Cubesat Space Protocol - A small network-layer protocol designed for Cubesats
Copyright (C) 2012 GomSpace ApS (http://www.gomspace.com)
Copyright (C) 2012 AAUSAT3 Project (http://aausat3.space.aau.dk)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef CSP_LT_RTABLE_H_
#define CSP_LT_RTABLE_H_

/**
   @file

   Routing table.

   The routing table maps a CSP destination address to an interface (and optional a via address).

   Normal routing: If the route's via address is set to #CSP_LT_NO_VIA_ADDRESS, the packet will be sent directly to the destination address
   specified in the CSP header, otherwise the packet will be sent the to the route's via address.
*/

#include <csp_lt_types.h>
#include <csp_lt_iflist.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
   No via address specified for the route, use CSP header destination.
*/
#define CSP_LT_NO_VIA_ADDRESS	0xFF

/**
   Legacy definition for #CSP_LT_NO_VIA_ADDRESS.
*/
#define CSP_LT_NODE_MAC	CSP_LT_NO_VIA_ADDRESS

/**
   Find route to address/node.
   @param[in] dest_address destination address.
   @return Route or NULL if no route found.
*/
csp_lt_route_t * csp_lt_rtable_find_route(csp_lt_inst_t *inst, uint8_t dest_address);

/**
   Set route to destination address/node.
   @param[in] dest_address destination address.
   @param[in] mask number of bits in netmask
   @param[in] ifc interface.
   @param[in] via assosicated via address.
   @return #CSP_LT_ERR_NONE on success, or an error code.
*/
int csp_lt_rtable_set(csp_lt_inst_t *inst, uint8_t dest_address, uint8_t mask, csp_lt_iface_t *ifc, uint8_t via);

void csp_lt_rtable_set_def_route(csp_lt_inst_t *inst, csp_lt_iface_t *iface);

#ifdef __cplusplus
}
#endif
#endif
