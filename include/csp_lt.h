#ifndef _CSP_LT_H_
#define _CSP_LT_H_

/* Includes */
#include <stdint.h>

#include <csp_lt_types.h>
#include <csp_lt_error.h>
#include <csp_lt_debug.h>
#include <csp_lt_iflist.h>
#include <csp_lt_rtable.h>

#ifdef __cplusplus
extern "C" {
#endif

int16_t csp_lt_init(csp_lt_inst_t *inst, uint8_t address);
int16_t csp_lt_reply(csp_lt_inst_t *inst);
int16_t csp_lt_send(csp_lt_inst_t *inst);
void csp_lt_rx_handler(csp_lt_inst_t *inst, uint8_t *buff, uint16_t len);

void csp_lt_set_hostname(csp_lt_inst_t *inst, const char *name);
void csp_lt_set_model(csp_lt_inst_t *inst, const char *model);
void csp_lt_set_revision(csp_lt_inst_t *inst, const char *rev);
const char *csp_lt_get_hostname(csp_lt_inst_t *inst);
const char *csp_lt_get_model(csp_lt_inst_t *inst);
const char *csp_lt_get_revision(csp_lt_inst_t *inst);

void *csp_lt_packet_data(csp_lt_inst_t *inst, size_t capacity);
csp_lt_packet_t* csp_lt_get_packet_ptr(csp_lt_inst_t *inst);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif // _CSP_LT_H_
