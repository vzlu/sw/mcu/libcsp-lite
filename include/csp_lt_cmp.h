#ifndef CSP_LT_CMP_H
#define CSP_LT_CMP_H

#include <csp_lt.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
   CMP type.
   @{
*/
/**
   CMP request.
*/
#define CSP_LT_CMP_REQUEST 0x00
/**
   CMP reply.
*/
#define CSP_LT_CMP_REPLY   0xff
/**@}*/

/**
   CMP requests.
   @{
*/
/**
   CMP request codes.
*/
/**
   Request identification, compile time, revision, hostname and model.
*/
#define CSP_LT_CMP_IDENT 1
/**
   Set/configure routing.
*/
#define CSP_LT_CMP_ROUTE_SET 2
/**
   Request interface statistics.
*/
#define CSP_LT_CMP_IF_STATS 3
/**
   Peek/read data from memory.
*/
#define CSP_LT_CMP_PEEK 4
/**
   Poke/write data from memory.
*/
#define CSP_LT_CMP_POKE 5
/**
   Get/set clock.
*/
#define CSP_LT_CMP_CLOCK 6
/**@}*/

/**
   CMP identification - max revision length.
*/
#define CSP_LT_CMP_IDENT_REV_LEN  20
/**
   CMP identification - max date length.
*/
#define CSP_LT_CMP_IDENT_DATE_LEN 12
/**
   CMP identification - max time length.
*/
#define CSP_LT_CMP_IDENT_TIME_LEN 9

/**
   CMP interface statistics - max interface name length.
*/
#define CSP_LT_CMP_ROUTE_IFACE_LEN 11

/**
   CMP peek/read memeory - max read length.
*/
#define CSP_LT_CMP_PEEK_MAX_LEN 200

/**
   CMP poke/write memeory - max write length.
*/
#define CSP_LT_CMP_POKE_MAX_LEN 200

/**
   Timestamp (cross platform).
*/
typedef struct {
        //! Seconds
	uint32_t tv_sec;
        //! Nano-seconds.
	uint32_t tv_nsec;
} csp_lt_timestamp_t;

/**
   CSP management protocol description.
*/
struct csp_lt_cmp_message {
        //! CMP request type.
        uint8_t type;
        //! CMP request code.
        uint8_t code;
	union {
		struct {
			char hostname[CSP_LT_HOSTNAME_LEN];
			char model[CSP_LT_MODEL_LEN];
			char revision[CSP_LT_CMP_IDENT_REV_LEN];
			char date[CSP_LT_CMP_IDENT_DATE_LEN];
			char time[CSP_LT_CMP_IDENT_TIME_LEN];
		} ident;
		struct {
			uint8_t dest_node;
			uint8_t next_hop_via;
			char interface[CSP_LT_CMP_ROUTE_IFACE_LEN];
		} route_set;
		struct __attribute__((__packed__)) {
			char interface[CSP_LT_CMP_ROUTE_IFACE_LEN];
			uint32_t tx;
			uint32_t rx;
			uint32_t tx_error;
			uint32_t rx_error;
			uint32_t drop;
			uint32_t autherr;
			uint32_t frame;
			uint32_t txbytes;
			uint32_t rxbytes;
			uint32_t irq;
		} if_stats;
		struct {
			uint32_t addr;
			uint8_t len;
			char data[CSP_LT_CMP_PEEK_MAX_LEN];
		} peek;
		struct {
			uint32_t addr;
			uint8_t len;
			char data[CSP_LT_CMP_POKE_MAX_LEN];
		} poke;
		csp_lt_timestamp_t clock;
	};
} __attribute__ ((packed));

/**
   Macro for calculating total size of management message.
*/
#define CMP_LT_SIZE(_memb) (sizeof(((struct csp_lt_cmp_message *)0)->type) + sizeof(((struct csp_lt_cmp_message *)0)->code) + sizeof(((struct csp_lt_cmp_message *)0)->_memb))

int csp_lt_cmp_handler(csp_lt_inst_t *inst);

#ifdef __cplusplus
}
#endif

#endif // CSP_LT_CMP_H
