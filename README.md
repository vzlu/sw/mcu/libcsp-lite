# libcsp-lite

Minimal port of libcsp, for embedded devices without RTOS.

# Dependencies
* tinylibc: https://gitlab.com/vzlu/sw/mcu/tinylibc

# Supported features
* Basic CSP using single buffer
* Default CSP services (ping, CMP etc.)
* Static routing
* Multiple interfaces
* CRC32

# Unsupported features
* Connections and sockets
* CIDR routing table format
* RDP, HMAC, XTEA, SFP
* Queues
* Promiscuous mode

# Compilation options
* CSP_LT_BUFF_SIZE - CSP packet buffer size
* CSP_LT_BIG_ENDIAN/CSP_LT_LITTLE_ENDIAN - specify endianness
* CSP_LT_PERSIST - specify linker section or macro to put variable into persistent storage (e.g. PROGMEM on AVR or \__attribute__ ((section (".persistent2"))) on LPC84x)
* CSP_LT_CRC_RAM_LUT - calculate CRC LUT into RAM and put it into RAM, can be used to save program memory
* CSP_LT_DEBUG - debug output via tinylibc when defined
